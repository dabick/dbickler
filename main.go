package main

import (
	"flag"
	"github.com/daaku/go.httpgzip"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"gitlab.com/dabick/go-util/dhttp"
	"gitlab.com/dabick/go-util/log"
	"gitlab.com/dabick/go-util/log/logserver"
	"gitlab.com/dabick/recipe"
	"gitlab.com/dabick/tictactoe"
	"net/http"
	"os"
	"strings"
)

func main() {
	dev := flag.Bool("dev", false, "Start server in development mode.")
	flag.Parse()

	r := mux.NewRouter()

	tictactoe.SetupTictactoeServer(r)
	recipe.RegisterRecipe(r)
	logserver.SetupLoggingServer(r)
	r.PathPrefix("/").Handler(interceptor{httpgzip.NewHandler(http.FileServer(http.Dir("./static/")))})

	accessFile, err := os.Create("access.log")
	if err != nil {
		log.Fatal(err)
	}
	accessFile.Chmod(0006)

	loggedRouter := handlers.LoggingHandler(accessFile, r)

	http.Handle("/", loggedRouter)

	if *dev {
		log.Println("Starting the server in debug mode.")
		devStart()
	} else {
		log.Println("Starting the server in production mode.")
		prodStart()
	}
}

type interceptor struct {
	http.Handler
}

func (c interceptor) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if strings.HasSuffix(r.URL.Path, ".html") {
		w.Header().Set("Cache-Control", "no-cache")
	} else if dhttp.NotModified(w, r) {
		return
	}
	c.Handler.ServeHTTP(w, r)
}

func devStart() {
	if err := http.ListenAndServe(":80", nil); err != nil {
		log.Fatal(err)
	}
}

func prodStart() {
	go func() {
		certDir := "/etc/letsencrypt/live/dbickler.com/"
		if err := http.ListenAndServeTLS(":443", certDir+"fullchain.pem", certDir+"privkey.pem", nil); err != nil {
			log.Fatal(err)
		}
	}()
	if err := http.ListenAndServe(":80", http.HandlerFunc(redir)); err != nil {
		log.Fatal(err)
	}
}

func redir(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://dbickler.com"+r.RequestURI, http.StatusMovedPermanently)
}
