function ajaxRequest(url, method, callback, error, body) {
    var request = new XMLHttpRequest();
    request.open(method, url, true);

    request.addEventListener("load", function(event) {
        if (request.status === 200 && request.getResponseHeader("Content-Length") > 0) {
            call(callback, JSON.parse(request.responseText));
        } else if (request.status >= 200 && request.status < 300) {
            call(callback, request.status);
        } else {
            call(error, request.status, request.responseText)
        }
    });
    request.addEventListener("error", function(event) {
        console.error('Error sending request: URL: %s Event: %o', url, event);
        call(error, 500);
    });
    request.addEventListener("abort", function(event) {
        console.log('Request aborted. URL: %s Event: %o', url, event);
        call(error, 500);
    });
    request.addEventListener("timeout", function(event) {
        console.error('Request timedout. URL: %s Event: %o', url, event);
        call(error, 500);
    });

    if (body) {
        request.send(JSON.stringify(body));
    } else {
        request.send();
    }
}

function call(callback, obj1, obj2) {
    if (callback) {
        callback(obj1, obj2);
    }
}

function getQueryParameter(paramName) {
    var query = window.location.search.substring(1);
    var vars = query.split('&');
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split('=');
        if (decodeURIComponent(pair[0]) == paramName) {
            return decodeURIComponent(pair[1]);
        }
    }
    console.log('Query Parameter %s not found', paramName);
    return null;
}

function show(overlayId, popupId) {
    if (popupId) {
        document.getElementById(popupId).style.display = 'block';
    }
    if (overlayId) {
        document.getElementById(overlayId).style.display = 'block';
    }
}

function hide(overlayId, popupId) {
    if (popupId) {
        document.getElementById(popupId).style.display = 'none';
    }
    if (overlayId) {
        document.getElementById(overlayId).style.display = 'none';
    }
}